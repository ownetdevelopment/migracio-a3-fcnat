﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CallWSNav
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                wsNAV.wsCreaDiario miws = new wsNAV.wsCreaDiario();
                miws.Url = "http://es-l-43kfr2v.iberia.grupotecnocom:8047/DynamicsNAV90/WS/CEDITARR/Codeunit/wsCreaDiario";

                miws.UseDefaultCredentials = true;
               

                MessageBox.Show(miws.CreaLineaDiario("GENERAL", "PREDET.", "PRUEBA", "ASFSFS", "20000", "EXT" + DateTime.Now.ToString(), DateTime.Today, Convert.ToDecimal(22.1)).ToString());
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                wsNAVFactura.wsFacturaVenta_Service miwsFactura = new wsNAVFactura.wsFacturaVenta_Service();

                miwsFactura.UseDefaultCredentials = true;
                //miwsFactura.Credentials = new System.Net.NetworkCredential("dominio\usurii", "minono..19");

                wsNAVFactura.wsFacturaVenta Factura = new wsNAVFactura.wsFacturaVenta();
                Factura.Posting_DateSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Factura.Posting_Date = Convert.ToDateTime("01/01/2017");// DateTime.Today;
                Factura.Posting_Description = "Nueva";
                Factura.External_Document_No = "Número externo";
                Factura.Sell_to_Customer_No = "20000";
                Factura.Sell_to_Customer_Name = "Mi nombre";

                //Crear el objeto factura
                miwsFactura.Create(ref Factura);

                //dimensionar la colección de filas
                Factura.SalesLines = new wsNAVFactura.Sales_Invoice_Subform_WS[2];

                //Creación de lineas bien por iteración o por asignación directa
                wsNAVFactura.Sales_Invoice_Subform_WS Linea= new wsNAVFactura.Sales_Invoice_Subform_WS();

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000001";
                Linea.Description = "Mi línea";
                Linea.Description_2 = "Mi línea desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 2;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = 12;

                //Añadir fila a la colección de filas
                Factura.SalesLines[0]= Linea;

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000002";
                Linea.Description = "Mi línea 2";
                Linea.Description_2 = "Mi línea 2 desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 13;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = Convert.ToDecimal(10.05);

                //Añadir fila a la colección de filas
                Factura.SalesLines[1] = Linea;

                //Actualizar el objeto factura
                miwsFactura.Update(ref Factura);





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                wsNAVFactura.wsFacturaVenta_Service miwsFactura = new wsNAVFactura.wsFacturaVenta_Service();

                miwsFactura.UseDefaultCredentials = true;
                //miws.Credentials = new System.Net.NetworkCredential(".\jesus", "minono..19");

                wsNAVFactura.wsFacturaVenta Factura = new wsNAVFactura.wsFacturaVenta();


                //Filtro
                List<wsNAVFactura.wsFacturaVenta_Filter> matrizfiltro = new List<wsNAVFactura.wsFacturaVenta_Filter>();
                wsNAVFactura.wsFacturaVenta_Filter nombreFiltro = new wsNAVFactura.wsFacturaVenta_Filter();
                nombreFiltro.Field = wsNAVFactura.wsFacturaVenta_Fields.No;
                nombreFiltro.Criteria = "*101*";
                matrizfiltro.Add(nombreFiltro);

                wsNAVFactura.wsFacturaVenta[]  list = miwsFactura.ReadMultiple(matrizfiltro.ToArray(), null, 100);
                foreach (wsNAVFactura.wsFacturaVenta fac in list)
                {
                 //   MessageBox.Show(fac.No);
                }

                dataGridView1.DataSource = list;
                
            }
            catch(Exception ex)
            {

            }
        }


    }
    
}

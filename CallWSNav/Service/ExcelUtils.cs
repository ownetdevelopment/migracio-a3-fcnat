﻿using Excel = Microsoft.Office.Interop.Excel;
using System;
using DTO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using Microsoft.Win32;

namespace Utils
{
    public class ExcelUtils
    {
        ILogger logger;
        Excel.Range xlRange;
        Excel.Application xlApp;
        Excel.Workbook xlWorkbook;
        Excel._Worksheet xlWorksheet;

        public Boolean ErrorState { get; set; } = false;

        public ExcelUtils(ILogger loggerMain)
        {
            logger = loggerMain;
        }

 

        public List<BillDto> parseFactures()
        {
            ErrorState = false;

            List<BillDto> llistaFactures = new List<BillDto>();
            BillDto Bill = null;
            BillLineDto BillLine = new BillLineDto();

            Int32 i = 2;
            String type = xlRange.Cells[i, 1].Value2.ToString();


            while (type != null)
            {
                try
                {

                    Bill = new BillDto();
                    Bill.Line = i;

                    Bill.BillDate = xlRange.Cells[i, 1].Value2.ToString();
                    try
                    {
                        Bill.ClubCode = xlRange.Cells[i, 2].Value2.ToString();                       
                    }
                    catch (Exception e)
                    {
                        Bill.ClubCode = "";
                    }

                    Bill.BillSerie = xlRange.Cells[i, 3].Value2.ToString();
                    Bill.BillReference = xlRange.Cells[i, 4].Value2.ToString();
                    Bill.BillAccountingAccount = xlRange.Cells[i, 6].Value2.ToString();
                    Bill.BillDescription = xlRange.Cells[i, 7].Value2.ToString();
                    Bill.BillImport = Convert.ToDecimal(xlRange.Cells[i, 8].Value2);
                    Bill.BillTypeIVA = xlRange.Cells[i, 9].Value2.ToString();
                    //Bill.BillBank = Int32.Parse(xlRange.Cells[i, 10].Value2);
                    Bill.BillCostCenter = xlRange.Cells[i, 11].Value2.ToString();
                    //Bill.BillCostCenter2 = xlRange.Cells[i, 12].Value2.ToString();
                    Bill.BillPaymentMethod = xlRange.Cells[i, 12].Value2.ToString();

                    logger.info("Important factura: " + Bill.BillCode);

                    llistaFactures.Add(Bill);

                    i++;
                    type = null;
                    if (xlRange.Cells[i, 1].Value2 != null)
                    {
                        type = xlRange.Cells[i, 1].Value2.ToString();
                    }
                }
                catch (Exception e)
                {
                    ErrorState = true;
                    logger.error("Fichero incongruente. Comprobar la línea: " + i);
                }
            }

            return llistaFactures;
        }

        public Excel.Range openFile()
        {
            
            // Displays an OpenFileDialog so the user can select a Cursor.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Excel |*.xlsx;*.xls";
            openFileDialog1.Title = "Selecciona Excel de Facturas";

            openFileDialog1.ShowDialog();
            

            xlApp = new Excel.Application();
            xlWorkbook = xlApp.Workbooks.Open(openFileDialog1.FileName);
            xlWorksheet = xlWorkbook.Sheets[1];
            xlRange = xlWorksheet.UsedRange;
            
            
            return xlRange;
        }


        public void CleanExcel()
        {
            
            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }


    }
}

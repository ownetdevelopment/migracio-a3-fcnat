﻿using System;
using DTO;
using System.Collections.Generic;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;

namespace Utils
{
    public class RESTUtils
    {
        ILogger logger;
        String restString = ConfigurationManager.AppSettings["restString"];
        String esbString = ConfigurationManager.AppSettings["esbString"];
        System.Windows.Controls.TextBox textBox { get; set; }

        
        public RESTUtils(ILogger loggerMain)
        {
            logger = loggerMain;
        }
       

        
        public List<ClientDto> getClients()
        {
            logger.info("Obteniendo lista clientes");

            List<ClientDto> ClientDto;
            using (var webClient = new System.Net.WebClient())
            {
                String json = webClient.DownloadString(esbString + "GetClientList");
                ClientDto = JsonConvert.DeserializeObject<List<ClientDto>>(json);
            }

            return ClientDto;
        }

        public List<DeliveryNotesDto> getPendingDeliveryNotes(Int32 idLiquidacioControl)
        {
            List<DeliveryNotesDto> DeliveryNotes = new List<DeliveryNotesDto>();
            using (var webClient = new System.Net.WebClient())
            {
                var settings = new JsonSerializerSettings
                {
                    
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                String json = webClient.DownloadString(esbString + "DeliveryNotes/getAllPending/" + idLiquidacioControl);
                DataResponseMessageDto drm = JsonConvert.DeserializeObject<DataResponseMessageDto>(json, settings);
                
                foreach(Object o in drm.messageData)
                {
                    DeliveryNotes.Add(JsonConvert.DeserializeObject<DeliveryNotesDto>(o.ToString(), settings));
                }
                
            }

            return DeliveryNotes;
        }

        public void markDeliveryNotesAsI(List<DeliveryNotesDto> DeliveryNotes)
        {
            Int32 idAlbara;
            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                idAlbara = dn.id;
                using (var webClient = new System.Net.WebClient())
                {
                    webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                    String json = JsonConvert.SerializeObject(dn);
                    String result = webClient.UploadString(esbString + "DeliveryNotes/markAsI", "POST", json);
                    
                }
            }
        }
        

    }
}

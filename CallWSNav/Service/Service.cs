﻿using System;
using DTO;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Controls;
using Utils;



namespace Services
{
    public class Service
    {
        //TextBox textBox { get; set; }
        String text;
        ILogger logger;

        
        
        public Service()
        {
            logger = new TextBoxLogger(null);
        }

        public Service(ILogger loggerInterface)
        {
            logger = loggerInterface;
        }

              

        public Boolean comprovarLlistaClients(List<BillDto> llistaFactures, List<ClientDto> ClientsDB)
        {
            Boolean Errors = false;
            Boolean Found;
            foreach (BillDto b in llistaFactures)
            {
                Found = false;
                logger.info("Validant " + b.ClubCode);
                foreach (ClientDto pro in ClientsDB)
                {
                    if (b.ClubCode.Equals(pro.CodCli))
                    {
                        Found = true;
                        break;
                    }
                }
                if (!Found)
                {
                    Errors = true;
                    if (!b.ClubCode.Equals(""))
                    {
                        logger.error("Línea: " + b.Line + " El cliente: " + b.ClubCode + " no encontrado.");
                    }
                    else
                    {
                        logger.error("Línea: " + b.Line + " El cliente no tiene informada la cuenta");
                    }

                }
            }
            return Errors;
        }

        public void crearFacturaFromDeliveryNote(DeliveryNotesDto deliveryNote)
        {
            try
            {


                wsNAVFactura.wsFacturaVenta_Service miwsFactura = new wsNAVFactura.wsFacturaVenta_Service();

                miwsFactura.UseDefaultCredentials = true;
                //miwsFactura.Credentials = new System.Net.NetworkCredential("dominio\usurii", "minono..19");

                wsNAVFactura.wsFacturaVenta Factura = new wsNAVFactura.wsFacturaVenta();
                Factura.Posting_DateSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Factura.Posting_Date = Convert.ToDateTime("01/01/2017");// DateTime.Today;
                Factura.Posting_Description = "Nueva";
                Factura.External_Document_No = "Número externo";
                Factura.Sell_to_Customer_No = "20000";
                Factura.Sell_to_Customer_Name = "Mi nombre";

                //Crear el objeto factura
                miwsFactura.Create(ref Factura);

                //dimensionar la colección de filas
                //aqui va el bucle de creacion de linias
                Factura.SalesLines = new wsNAVFactura.Sales_Invoice_Subform_WS[2];

                //Creación de lineas bien por iteración o por asignación directa
                wsNAVFactura.Sales_Invoice_Subform_WS Linea = new wsNAVFactura.Sales_Invoice_Subform_WS();

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000001";
                Linea.Description = "Mi línea";
                Linea.Description_2 = "Mi línea desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 2;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = 12;

                //Añadir fila a la colección de filas
                Factura.SalesLines[0] = Linea;

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000002";
                Linea.Description = "Mi línea 2";
                Linea.Description_2 = "Mi línea 2 desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 13;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = Convert.ToDecimal(10.05);

                //Añadir fila a la colección de filas
                Factura.SalesLines[1] = Linea;

                //Actualizar el objeto factura
                miwsFactura.Update(ref Factura);

            }
            catch (Exception ex)
            {
                logger.error(ex.Message);
            }
            

        }


        public void crearFacturaClubsBatch(List<BillDto> llistaFactures, String user, String pass, String empresa)
        {
            /*
            try
            {
                wsNAVFactura.wsFacturaVenta_Service miwsFactura = new wsNAVFactura.wsFacturaVenta_Service();

                miwsFactura.UseDefaultCredentials = true;
                //miwsFactura.Credentials = new System.Net.NetworkCredential("dominio\usurii", "minono..19");

                wsNAVFactura.wsFacturaVenta Factura = new wsNAVFactura.wsFacturaVenta();
                Factura.Posting_DateSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Factura.Posting_Date = Convert.ToDateTime("01/01/2017");// DateTime.Today;
                Factura.Posting_Description = "Nueva";
                Factura.External_Document_No = "Número externo";
                Factura.Sell_to_Customer_No = "20000";
                Factura.Sell_to_Customer_Name = "Mi nombre";

                //Crear el objeto factura
                miwsFactura.Create(ref Factura);

                //dimensionar la colección de filas
                //aqui va el bucle de creacion de linias
                Factura.SalesLines = new wsNAVFactura.Sales_Invoice_Subform_WS[2];

                //Creación de lineas bien por iteración o por asignación directa
                wsNAVFactura.Sales_Invoice_Subform_WS Linea = new wsNAVFactura.Sales_Invoice_Subform_WS();

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000001";
                Linea.Description = "Mi línea";
                Linea.Description_2 = "Mi línea desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 2;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = 12;

                //Añadir fila a la colección de filas
                Factura.SalesLines[0] = Linea;

                //Tipo linea cuenta
                Linea.Type = wsNAVFactura.Type.G_L_Account;
                Linea.TypeSpecified = true; //los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.No = "7000002";
                Linea.Description = "Mi línea 2";
                Linea.Description_2 = "Mi línea 2 desc 2";
                Linea.QuantitySpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Quantity = 13;
                Linea.Unit_PriceSpecified = true;//los datos que no sean de tipo texto se deben marcar como especificado con esta propiedad
                Linea.Unit_Price = Convert.ToDecimal(10.05);

                //Añadir fila a la colección de filas
                Factura.SalesLines[1] = Linea;

                //Actualizar el objeto factura
                miwsFactura.Update(ref Factura);
                
            }
            catch (Exception ex)
            {
                logger.error(ex.Message);
            }
            */
        }

       

       
    }
}

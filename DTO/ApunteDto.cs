﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ApunteDto
    {
        public Int32 idApunt { get; set; }
        public String customerName { get; set; }
        public String date { get; set; }
        public String valueDate { get; set; }
        public String compteContable { get; set; }
        public String description { get; set; }
        public Decimal debitImport { get; set; }
        public Decimal havingImport { get; set; }
        public Double balance { get; set; }
        public String centroCoste1 { get; set; }
        public String centroCoste2 { get; set; }
        public String centroCoste3 { get; set; }
    }
}

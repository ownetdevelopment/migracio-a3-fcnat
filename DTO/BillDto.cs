﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BillDto
    {
        public Int32 Line { get; set; }
        public String ClubCode { get; set; }
        public String BillCode { get; set; }
        public String BillDate { get; set; }
        public String BillValueDate { get; set; }
        public String BillExpirationDate { get; set; }
        public Decimal BillImport { get; set; }
        public String BillSerie { get; set; }
        public String BillReference { get; set; }
        public String BillAccountingAccount { get; set; }
        public String BillDescription { get; set; }
        public String BillTypeIVA { get; set; }
        public Int32 BillBank { get; set; }        
        public String BillCostCenter { get; set; }
        public String BillCostCenter2 { get; set; }
        public String BillPaymentMethod { get; set; }

        public List<BillLineDto> BillLines {get; set; }

    }
}

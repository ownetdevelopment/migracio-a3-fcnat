﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BillLineDto
    {
        public Int32 Line { get; set; }
        public String BillLineType { get; set; }
        public String BillLineDate { get; set; }
        public String BillLineDescription { get; set; }
        public Decimal BillLineFee { get; set; }
        public Decimal BillLineCommuting { get; set; }
        public Decimal BillLineOthers { get; set; }        
        public Decimal BillLineTotal { get; set; }
        public String BillLineConcept { get; set; }

    }
}

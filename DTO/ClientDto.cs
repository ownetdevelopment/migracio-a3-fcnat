﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ClientDto
    {
        public String CodCli { get; set; }
        public String NomCli { get; set; }
    }
}

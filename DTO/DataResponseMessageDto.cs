﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DataResponseMessageDto
    {
        public String result { get; set; }
        public String errorCode { get; set; }
        public String message { get; set; }
        public List<Object> messageData { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DateTimeOW
    {
        public DateTime Date { get; set; }

        public DateTimeOW()
        {
            
            this.Date = DateTime.Now;
        }
        public DateTimeOW(DateTime D)
        {
            this.Date = D;
        }
        public DateTimeOW(String D)
        {
            this.Date = DateTime.ParseExact(D, "yyyy-MM-dd", null);
        }

        
        public String ToSqlString()
        {
            return this.Date.ToString("dd/MM/yyyy");
        }
    }
}

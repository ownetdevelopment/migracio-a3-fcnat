﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DeliveryNotesDetailsDto
    {
        public Int32 id { get; set;}
        public Int32 idGame { get; set; }
        public Int32 idBill { get; set; }
        public Int32 collector { get; set; }
        public String descriptionDetail { get; set; }
        public Decimal amountDetail { get; set; }
        public Int32 idBillConcept { get; set; }
        public String accountingAccount { get; set; }
        public String concept { get; set; }



    }
}

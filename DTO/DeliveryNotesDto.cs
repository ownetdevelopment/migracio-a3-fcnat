﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DeliveryNotesDto
    {

        public Int32 id { get; set; }
        public Int32 idClub { get; set; }
        public String clientCode { get; set; }
        public Int32 billNumber { get; set; }
        public DateTime accountingDate { get; set; }
        public DateTime billDate { get; set; }
        public String description { get; set; }
        public Decimal totalAmount { get; set; }
        public String clientName { get; set; }
        public Int32 season { get; set; }
        public Int32 zone { get; set; }
        public Decimal amountIva { get; set; }
        public Decimal amountNoIva { get; set; }
        public Int32 typeIva { get; set; }
        public Int32 idRefereeLiquidationControl { get; set; }
        public Int32 counterpartDeposit { get; set; }
        public DateTime transferDate { get; set; }
        public Int32 idBillA3 { get; set; } = -1;
        public String transferState { get; set; }
        public DateTime unsuscriptionDate { get; set; }
        public Int32 idDeliveryNoteSerie { get; set; }
        public String deliveryNoteSerie { get; set; }
        public String paymentMethod { get; set; }
        public Int32 idCostCenter { get; set; }
        public String costCenter { get; set; }
        public String transferType { get; set; }
        public Int32 paid { get; set; }
        public Int32 transferUser { get; set; }
        public Int32 savingUser { get; set; }
        public List<DeliveryNotesDetailsDto> deliveryNoteDetails { get; set; }

    }
}

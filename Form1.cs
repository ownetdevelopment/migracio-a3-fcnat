﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using Services;
using Utils;
using System.Configuration;
using CallWSNav1;
using System.Reflection;
using CallWSNav1.Utils;

namespace CallWSNav1
{
    public partial class Form1 : Form
    {
        String version = "1.0.2";
        ExcelUtils ExcelService;
        DBUtils DBUtils;
        RESTUtils RESTUtils;
        IServices Service;
        ILogger logger;
        ERPUtils erpUtils = new ERPUtils();

        Boolean Error;
        Boolean ErrorExcel;

        public Form1()
        {
            InitializeComponent();
            

            String ExecutionMode = ConfigurationManager.AppSettings["executionMode"];

            //FACTURA DAO
            Assembly assembly = typeof(IServices).Assembly;
            Type type = assembly.GetType("Services.Service" + ExecutionMode + "Impl");
            Service = (IServices)Activator.CreateInstance(type);

            logger = new TextBoxLogger(textBox);
            Service.LinkILogger(logger);
            DBUtils = new DBUtils(logger);
            RESTUtils = new RESTUtils(logger);
            ExcelService = new ExcelUtils(logger);

            this.button1.Visible = false;
            this.button2.Visible = false;
            this.button4.Visible = false;
            this.button5.Visible = false;
            this.button6.Visible = false;
            this.button7.Visible = false;
            this.numericUpDown1.Visible = false;
            this.numericUpDown2.Visible = false;
            this.label1.Visible = false;
            this.label2.Visible = false;
            this.comboBox1.Visible = false;
            label4.Text = version;

            switch (ConfigurationManager.AppSettings["federation"])
            {
                case "FCN":
                    this.pictureBox1.BackgroundImage = global::CallWSNav1.Properties.Resources.logo_fcn;
                    this.label1.Visible = true;
                    this.comboBox1.Visible = true;
                    this.button7.Visible = true;
                    break;

            }
        }


        //TRASPASO ALBARANES FBCYL
        private void button2_Click(object sender, EventArgs e)
        {

            logger.info("Recuperando albaranes pendientes de traspaso");
            List<DeliveryNotesDto> DeliveryNotes = RESTUtils.getPendingDeliveryNotes("PROD");
            logger.info("Albaranes a traspasar:" + DeliveryNotes.Count);

            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                //marquem l'Albarà com a Iniciat
                logger.info("Albarán "+dn.id+" correspondiente a "+dn.clientName+": PROCESANDO");
                RESTUtils.markDeliveryNote(dn, "I", "PROD");

                DeliveryNotesDto aux = Service.crearFacturaFromDeliveryNote(dn, null, null, null, null, null);

                //si tot ha anat bé marcarem l'Albarà com a Finalitzat
                logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": FINALIZADO");
                RESTUtils.markDeliveryNote(aux, "F", "PROD");
            }
            logger.info("Proceso finalizado");

        }

       
        //FACTURES DESDE EXCEL
        private void button1_Click(object sender, EventArgs e)
        {
            Error = false;
            logger.info("Inicializando...");
            /*
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("La empresa no está informada");
                Error = true;
            }
            */


            if (!Error)
            {
                String empresa = comboBox1.SelectedItem.ToString();
                    
                try
                {
                    ExcelService.openFile();
                    logger.info("Abriendo fichero...");
                    List<BillDto> llistaFactures = ExcelService.parseFactures();
                    ErrorExcel = ExcelService.ErrorState;

                    ExcelService.CleanExcel();

                    List<ClientDto> ClientsDB = RESTUtils.getClients();

                    logger.newLine();
                    logger.info("Comprobando clientes...");
                    Boolean Errors = RESTUtils.comprovarLlistaClients(llistaFactures, ClientsDB);

                    if (!Errors && !ErrorExcel)
                    {
                        logger.info("Facturas a generar: " + llistaFactures.Count);
                        logger.newLine();
                        logger.info("Insertando facturas...");
                        foreach(BillDto b in llistaFactures)
                        {
                            Service.crearFactura(b, empresa);
                        }                        
                    }
                    else
                    {
                        logger.info("Existen errores, no se ha realizado ninguna acción");
                    }
                }
                catch (Exception q)
                {
                    logger.info(q.Message);
                }
            }
            logger.info("Proceso finalizado");
            logger.newLine();

            GC.Collect();
            GC.WaitForPendingFinalizers();

        }


        //DIPOSITS ARBITRALS
        private void button4_Click(object sender, EventArgs e)
        {
            Int32 idDipositControl = Decimal.ToInt32(numericUpDown1.Value);
            String empresa = comboBox1.SelectedItem.ToString();

            logger.info("Recuperando depósitos pendientes de traspaso");
            List<DeliveryNotesDto> DeliveryNotes = RESTUtils.getPendingDeliveryNotesFromDiposits(idDipositControl, empresa);
            logger.info("Depósitos a traspasar:" + DeliveryNotes.Count);
            Service.Initialize(empresa);

            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                //marquem l'Albarà com a Iniciat
                logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": PROCESANDO");
                RESTUtils.markDeliveryNote(dn, "I", empresa);

                try
                {                    
                    DeliveryNotesDto aux = Service.crearFacturaFromDeliveryNote(dn, "D", "1", dn.deliveryNoteSerie, dn.description, "AREAARB");
                    //si tot ha anat bé marcarem l'Albarà com a Finalitzat
                    logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": FINALIZADO");
                    RESTUtils.markDeliveryNote(aux, "F", empresa);
                }                
                catch (Exception ex)
                {
                    logger.error(ex.ToString());
                }
                
            }
            Service.Destroy();
            logger.info("Proceso finalizado");
        }

        //ALTRES ALBARANS
        private void button5_Click(object sender, EventArgs e)
        {
            String empresa = comboBox1.SelectedItem.ToString();

            logger.info("Recuperando otros albaranes pendientes de traspaso");
            List<DeliveryNotesDto> DeliveryNotes = RESTUtils.getPendingOtherDeliveryNotes(empresa);
            logger.info("Albaranes a traspasar:" + DeliveryNotes.Count);
            Service.Initialize(empresa);

            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                //marquem l'Albarà com a Iniciat
                logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": PROCESANDO");
                RESTUtils.markDeliveryNote(dn, "I", empresa);
                try
                {
                    String banco = dn.deliveryNoteSerie == "1/18" || dn.deliveryNoteSerie == "6/18" ? "3":"1";
                    String centroCoste = dn.idCostCenter == 1 ? "1" : dn.costCenter;
                    DeliveryNotesDto aux = Service.crearFacturaFromDeliveryNote(dn, "TR", banco, dn.deliveryNoteSerie, dn.description, centroCoste);
                    //si tot ha anat bé marcarem l'Albarà com a Finalitzat
                    logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": FINALIZADO");
                    RESTUtils.markDeliveryNote(aux, "F", empresa);
                }
                catch (Exception ex)
                {
                    logger.error(ex.ToString());
                }
            }
            Service.Destroy();
            logger.info("Proceso finalizado");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Int32 idLiquidacioArbitralControl = Decimal.ToInt32(numericUpDown2.Value);
            String empresa = comboBox1.SelectedItem.ToString();

            logger.info("Recuperando albaranes de liq arbitrales sin prorrata pendientes de traspaso");
            List<DeliveryNotesDto> DeliveryNotes = RESTUtils.getPendingDeliveryNotesFromRefereeLiquidationWithNoProrate(idLiquidacioArbitralControl, empresa);
            logger.info("Liquidaciones a traspasar:" + DeliveryNotes.Count);
            Service.Initialize(empresa);

            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                //marquem l'Albarà com a Iniciat
                logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": PROCESANDO");
                RESTUtils.markDeliveryNote(dn, "I", empresa);

                try
                {
                    DeliveryNotesDto aux = Service.crearFacturaFromDeliveryNote(dn, "D", "1", dn.deliveryNoteSerie, dn.description, "AREAARB");
                    //si tot ha anat bé marcarem l'Albarà com a Finalitzat
                    logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": FINALIZADO");
                    RESTUtils.markDeliveryNote(aux, "F", empresa);
                }
                catch (Exception ex)
                {
                    logger.error(ex.ToString());
                }

            }
            Service.Destroy();
            logger.info("Proceso finalizado");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            //Int32 idLiquidacioArbitralControl = Decimal.ToInt32(numericUpDown2.Value);
            String empresa = comboBox1.SelectedItem.ToString();
           
            logger.info("Recuperando albaranes de liq arbitrales sin prorrata pendientes de traspaso");
            List<DeliveryNotesDto> DeliveryNotes = RESTUtils.getPendingDeliveryNotes(empresa);
            logger.info("Liquidaciones a traspasar:" + DeliveryNotes.Count);
            //Service.Initialize(empresa);

            foreach (DeliveryNotesDto dn in DeliveryNotes)
            {
                //marquem l'Albarà com a Iniciat
                logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": PROCESANDO");
                RESTUtils.markDeliveryNote(dn, "I", empresa);

                try
                {
                    DeliveryNotesDto aux = Service.crearFacturaFromDeliveryNote(dn, "TR", "1", dn.deliveryNoteSerie, dn.description, "1");
                    //si tot ha anat bé marcarem l'Albarà com a Finalitzat
                    logger.info("Albarán " + dn.id + " correspondiente a " + dn.clientName + ": FINALIZADO");
                    RESTUtils.markDeliveryNote(aux, "F", empresa);
                }
                catch (Exception ex)
                {
                    logger.error(ex.ToString());
                }

            }
            Service.Destroy();
            logger.info("Proceso finalizado");
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    
}

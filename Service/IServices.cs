﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using Utils;

namespace Services
{
    interface IServices
    {
        void Initialize(String empresa);

        void LinkEmptyLogger();

        void LinkILogger(ILogger loggerInterface);

        DeliveryNotesDto crearFacturaFromDeliveryNote(DeliveryNotesDto deliveryNote, String documentoPago, String codigoBanco, String serie, String referencia, String centroCoste);

        void crearFactura(BillDto factura, String empresa);

        void Destroy();
    }
}

﻿using System;
using DTO;
using System.Collections.Generic;
using Utils;
using System.Windows.Forms;
using System.Configuration;
using System.Text;
using CallWSNav1.Utils;
using a3ERPActiveX;

namespace Services
{
    public partial class ServiceA3Impl : IServices
    {
        //TextBox textBox { get; set; }
        String text;
        ILogger logger;
        ERPUtils erpUtils = new ERPUtils();
        String federation = ConfigurationManager.AppSettings["federation"];

        protected Enlace Enllas;

        public void Initialize(String empresa)
        {
            Enllas = new a3ERPActiveX.Enlace();
            Enllas.LoginUsuario("AUTOMATIC", "AUTOMATIC");
            text = Enllas.sMensaje;
            logger.info("Mensage de la conexion" + text);
            logger.info("Intentando iniciar conexion A3");
            Enllas.Iniciar(empresa, null);
            logger.info("Resultado de iniciar Conexion " + Enllas.sMensaje);
        }

        public void Destroy()
        {
            Enllas.Acabar();
        }

        public void LinkEmptyLogger()
        {
            logger = new TextBoxLogger(null);
        }

        public void LinkILogger(ILogger loggerInterface)
        {
            logger = loggerInterface;
        }

        
        public DeliveryNotesDto crearFacturaFromDeliveryNote(DeliveryNotesDto deliveryNote, String documentoPago, String codigoBanco, String serie, String referencia, String centroCoste)
        {
            
            try
            {

                if (deliveryNote.amountNoIva==0  &&  deliveryNote.amountIva== 0){
                    deliveryNote.idBillA3 = Decimal.ToInt32(0);
                    return deliveryNote;
                }


                logger.info("Creant factura per " + deliveryNote.clientCode);
                Factura Factura = new Factura();
                Factura.Iniciar();
                text = Enllas.sMensaje; 
                //Factura.Nuevo(b.BillDate, "   "+b.ClubCode, false, true, true, true);
                //Factura.Nuevo(new DateTimeOW().ToSqlString(), "   " + b.ClubCode, false, true, true, true);

                //Double dataDouble = Double.Parse(deliveryNote.billDate);
                //DateTimeOW dataOW = new DateTimeOW(deliveryNote.billDate);
                //String data = dataOW.ToSqlString();              
                String dataAvui = String.Format("{0:dd/MM/yyyy}", DateTime.Today);

                String codiClient = deliveryNote.clientCode;
                while(codiClient.Length < 8)
                {
                    codiClient = " " + codiClient;
                }
                Factura.Nuevo(dataAvui, codiClient, false, true, true, true);

                // PREPARACIÓ PER TAL DE FIXAR LA DATA DE LA FACTURA A LA DATA DE TRASPAS                
                Factura.set_AsStringCab("FECHACONTABLE", dataAvui);
                Factura.set_AsStringCab("FORPAG", "C");
                Factura.set_AsStringCab("REFERENCIA", erpUtils.replaceAccents(referencia));
                Factura.set_AsStringCab("DOCPAG", documentoPago);
                Factura.set_AsStringCab("CENTROCOSTE", centroCoste);
                //Factura.set_AsStringCab("CENTROCOSTE2", centreCost2Club);
                Factura.set_AsStringCab("SERIE", serie);
                Factura.set_AsStringCab("CODBAN", codigoBanco);

                foreach (DeliveryNotesDetailsDto deliveryNoteDetail in deliveryNote.deliveryNoteDetails)
                {
                    Factura.NuevaLinea();

                    Factura.set_AsFloatLin("UNIDADES", 1.0);
                    Factura.set_AsStringLin("CTACONL", deliveryNoteDetail.accountingAccount);
                    if (federation.Equals("FBIB")){
                        Factura.set_AsStringLin("DESCLIN", erpUtils.replaceAccents(deliveryNoteDetail.descriptionDetail + " - " + deliveryNoteDetail.concept));
                    }else{ 
                        Factura.set_AsStringLin("DESCLIN", erpUtils.replaceAccents(deliveryNoteDetail.descriptionDetail));
                    }
                    //Factura.set_AsFloatLin("PRECIO", Convert.ToDouble(deliveryNoteDetail.amountDetail));
                    Factura.set_AsFloatLin("PRCMONEDA", Convert.ToDouble(deliveryNoteDetail.amountDetail));
                    Factura.set_AsStringLin("TIPIVA", "EXE");
                    Factura.AnadirLinea();
                }
                Decimal numeroFactura = Factura.Anade();
                Factura.Acabar();
                logger.info("Factura número " + numeroFactura);
                logger.newLine();

                deliveryNote.idBillA3 = Decimal.ToInt32(numeroFactura);


                text = Enllas.sMensaje;

            }
            catch (Exception ex)
            {
                logger.error(ex.ToString());
                throw new Exception(ex.ToString());

            }

            

            return deliveryNote;
        }


        public void crearFactura(BillDto factura, String empresa)
        {
 
        }




    }
}

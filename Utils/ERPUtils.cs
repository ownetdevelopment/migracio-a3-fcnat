﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CallWSNav1.Utils
{
    class ERPUtils
    {
        public String replaceAccents(String toReplace)
        {
            /* 
             * var srcEncoding = Encoding.UTF8; // utf-8
           var destEncoding = Encoding.GetEncoding(1252); // windows-1252

           // convert the source bytes to the destination bytes
           var destBytes = Encoding.Convert(srcEncoding, destEncoding, srcEncoding.GetBytes(toReplace));

           // process the byte[]

           var destString = destEncoding.GetString(destBytes); // ... get the string

           return destString;
          */

            //vocals minúscules accents tancats
            toReplace = toReplace.Replace("Ã¡", "á");
            toReplace = toReplace.Replace("Ã©", "é");
            toReplace = toReplace.Replace("Ã­", "í"); //OJOOOO: la I en utf-8 té un caràcter ocult           
            toReplace = toReplace.Replace("Ã³", "ó");
            toReplace = toReplace.Replace("Ãº", "ú");

            //vocals minúscules accents oberts
            toReplace = toReplace.Replace("Ã ", "à");
            toReplace = toReplace.Replace("Ã¨", "è");
            toReplace = toReplace.Replace("Ã¬", "ì");
            toReplace = toReplace.Replace("Ã²", "ò");
            toReplace = toReplace.Replace("Ã¹", "ù");

            //vocals majúscules accents tancats
            toReplace = toReplace.Replace("Ã\u0081", "Á");
            toReplace = toReplace.Replace("Ã‰", "É");
            toReplace = toReplace.Replace("Ã\u008d", "Í");
            toReplace = toReplace.Replace("Ã“", "Ó");
            toReplace = toReplace.Replace("Ãš", "Ú");

            //vocals majúscules accents oberts
            toReplace = toReplace.Replace("Ã€", "À");
            toReplace = toReplace.Replace("Ãˆ", "È");
            toReplace = toReplace.Replace("ÃŒ", "Ì");
            toReplace = toReplace.Replace("Ã’", "Ò");
            toReplace = toReplace.Replace("Ã™", "Ù");

            //Ñ         
            toReplace = toReplace.Replace("Ã±", "ñ");
            toReplace = toReplace.Replace("Ã‘", "Ñ");

            //ç
            toReplace = toReplace.Replace("Ã§", "ç");

            //Símbols         
            toReplace = toReplace.Replace("Âº", "º");
            toReplace = toReplace.Replace("Âª", "ª");
            toReplace = toReplace.Replace("Â·", "·");

            

            return toReplace;

        }

        public String convertEmpresaToESBString(String empresa)
        {
            String esbString;
            String federacio = ConfigurationManager.AppSettings["federation"];
            if (empresa.Equals(federacio))
            {
                esbString = ConfigurationManager.AppSettings["esbString" + "Prod" + federacio];
            }else if (empresa.Equals(federacio + "_DES")){
                esbString = ConfigurationManager.AppSettings["esbString" + "Des" + federacio];
            }
            else
            {
                throw new Exception("empresa no seleccionada");
            }


            return esbString;
        }

        public String convertEmpresaToEmpresaA3ERP(String empresa)
        {
            String empresaERP;
            String federacio = ConfigurationManager.AppSettings["federation"];
            if (empresa.Equals(federacio))
            {
                empresaERP = federacio;
            }
            else if (empresa.Equals("DES"))
            {
                empresaERP = federacio + "_DES";
            }
            else
            {
                throw new Exception("empresa no seleccionada");
            }


            return empresaERP;
        }
    }
}

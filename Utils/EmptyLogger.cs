﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Utils
{
    class EmptyLogger : ILogger
    {

        public void error(string text)
        {

        }

        public void info(string text)
        {

        }

        public void warn(string text)
        {

        }

        public void newLine()
        {

        }
    }
}

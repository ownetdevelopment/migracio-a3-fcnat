﻿using System;

namespace Utils
{
    public interface ILogger
    {
        void warn(String text);

        void info(String text);

        void error(String text);

        void newLine();
    }
}

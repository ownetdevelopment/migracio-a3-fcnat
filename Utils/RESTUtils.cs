﻿using System;
using DTO;
using System.Collections.Generic;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;
using System.Windows.Forms;
using CallWSNav1.Utils;

namespace Utils
{
    public class RESTUtils
    {
        ILogger logger;
        String restString = ConfigurationManager.AppSettings["restString"];
        ERPUtils erpUtils = new ERPUtils();
        
        TextBox textBox { get; set; }

        
        public RESTUtils(ILogger loggerMain)
        {
            logger = loggerMain;
        }
       

        
        public List<ClientDto> getClients()
        {
            logger.info("Obteniendo lista clientes");

            List<ClientDto> ClientDto;
            using (var webClient = new System.Net.WebClient())
            {
                String json = webClient.DownloadString(restString + "GetClientList");
                ClientDto = JsonConvert.DeserializeObject<List<ClientDto>>(json);
            }

            return ClientDto;
        }

        public List<DeliveryNotesDto> getPendingDeliveryNotes(String empresa)
        {
            List<DeliveryNotesDto> DeliveryNotes = new List<DeliveryNotesDto>();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            using (var webClient = new System.Net.WebClient())
            {
                var settings = new JsonSerializerSettings
                {
                    
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                String url = erpUtils.convertEmpresaToESBString(empresa) + "DeliveryNotes/getAllPendingLiquidationControlUseless/";

                String json = webClient.DownloadString(url);
                DataResponseMessageDto drm = JsonConvert.DeserializeObject<DataResponseMessageDto>(json, settings);
                
                foreach(Object o in drm.messageData)
                {
                    DeliveryNotes.Add(JsonConvert.DeserializeObject<DeliveryNotesDto>(o.ToString(), settings));
                }
                
            }

            return DeliveryNotes;
        }

        public List<DeliveryNotesDto> getPendingDeliveryNotesFromDiposits(Int32 idDipositControl, String empresa)
        {
            List<DeliveryNotesDto> DeliveryNotes = new List<DeliveryNotesDto>();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            using (var webClient = new System.Net.WebClient())
            {
                var settings = new JsonSerializerSettings
                {

                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                
                String url = erpUtils.convertEmpresaToESBString(empresa) + "DeliveryNotes/getPendingDeliveryNotesFromDiposits/"+ idDipositControl;

                String json = webClient.DownloadString(url);
                DataResponseMessageDto drm = JsonConvert.DeserializeObject<DataResponseMessageDto>(json, settings);

                foreach (Object o in drm.messageData)
                {
                    DeliveryNotes.Add(JsonConvert.DeserializeObject<DeliveryNotesDto>(o.ToString(), settings));
                }

            }

            return DeliveryNotes;
        }

        public List<DeliveryNotesDto> getPendingDeliveryNotesFromRefereeLiquidationWithNoProrate(Int32 idLiquidacioArbitralControl, String empresa)
        {
            List<DeliveryNotesDto> DeliveryNotes = new List<DeliveryNotesDto>();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            using (var webClient = new System.Net.WebClient())
            {
                var settings = new JsonSerializerSettings
                {

                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                String url = erpUtils.convertEmpresaToESBString(empresa) + "DeliveryNotes/getPendingDeliveryNotesFromRefereeLiquidationWithNoProrate/" + idLiquidacioArbitralControl;

                String json = webClient.DownloadString(url);
                DataResponseMessageDto drm = JsonConvert.DeserializeObject<DataResponseMessageDto>(json, settings);

                foreach (Object o in drm.messageData)
                {
                    DeliveryNotes.Add(JsonConvert.DeserializeObject<DeliveryNotesDto>(o.ToString(), settings));
                }

            }

            return DeliveryNotes;
        }

        

        public List<DeliveryNotesDto> getPendingOtherDeliveryNotes(String empresa)
        {
            List<DeliveryNotesDto> DeliveryNotes = new List<DeliveryNotesDto>();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12; 
            using (var webClient = new System.Net.WebClient())
            {
                var settings = new JsonSerializerSettings
                {

                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                String url = erpUtils.convertEmpresaToESBString(empresa) + "DeliveryNotes/getPendingOtherDeliveryNotes/";

                String json = webClient.DownloadString(url);
                DataResponseMessageDto drm = JsonConvert.DeserializeObject<DataResponseMessageDto>(json, settings);

                foreach (Object o in drm.messageData)
                {
                    DeliveryNotes.Add(JsonConvert.DeserializeObject<DeliveryNotesDto>(o.ToString(), settings));
                }

            }

            return DeliveryNotes;
        }



        public void markDeliveryNote(DeliveryNotesDto dn, String State, String empresa)
        {
            
            using (var webClient = new System.Net.WebClient())
            {
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";
                String json = JsonConvert.SerializeObject(dn);
                if(State != null)
                {
                    String result = webClient.UploadString(erpUtils.convertEmpresaToESBString(empresa) + "DeliveryNotes/markAs"+State, "POST", json);
                }
                    
            }
            
        }

        public Boolean comprovarLlistaClients(List<BillDto> llistaFactures, List<ClientDto> ClientsDB)
        {
            Boolean Errors = false;
            Boolean Found;
            foreach (BillDto b in llistaFactures)
            {
                Found = false;
                logger.info("Validant " + b.ClubCode);
                foreach (ClientDto pro in ClientsDB)
                {
                    if (b.ClubCode.Equals(pro.CodCli))
                    {
                        Found = true;
                        break;
                    }
                }
                if (!Found)
                {
                    Errors = true;
                    if (!b.ClubCode.Equals(""))
                    {
                        logger.error("Línea: " + b.Line + " El cliente: " + b.ClubCode + " no encontrado.");
                    }
                    else
                    {
                        logger.error("Línea: " + b.Line + " El cliente no tiene informada la cuenta");
                    }

                }
            }
            return Errors;
        }
    }
}

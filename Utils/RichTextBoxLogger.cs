﻿using System.Windows.Forms;

namespace Utils
{

    class RichTextBoxLogger : ILogger
    {
        RichTextBox richTextBox;

        public RichTextBoxLogger(RichTextBox textBoxMain)
        {
            richTextBox = textBoxMain;
        }

        public void error(string text)
        {            
            richTextBox.AppendText("error: " + text);
            newLine();
        }

        public void info(string text)
        {
            richTextBox.AppendText("info: "+text);
            newLine();
        }

        public void warn(string text)
        {
            richTextBox.AppendText("warn: " + text);
            newLine();
        }


        public void newLine()
        {
            richTextBox.AppendText("\n");
            //richTextBox.ScrollToEnd();
        }
    }
}

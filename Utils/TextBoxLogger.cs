﻿using System.Windows.Forms;

namespace Utils
{

    class TextBoxLogger : ILogger
    {
        TextBox textBox;

        public TextBoxLogger(TextBox textBoxMain)
        {
            textBox = textBoxMain;
        }

        public void error(string text)
        {
            textBox.AppendText("error: " + text);
            newLine();
        }

        public void info(string text)
        {
            textBox.AppendText("info: "+text);
            newLine();
        }

        public void warn(string text)
        {
            textBox.AppendText("warn: " + text);
            newLine();
        }

        public void newLine()
        {
            textBox.AppendText("\n");
            //textBox.ScrollToEnd();
        }
    }
}
